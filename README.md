## Introduction

### Description

Telegram bot for home surveillance (of grandkitties).

### Commands

- `\start`: show bot information.
- `\photo`: capture photo.
- `\video`: capture video.
- `\trivia`: send trivia from text file.

## Hardware

- Raspberry Pi Zero/2/3.
- Raspberry Pi Camera Module V1/V2.

## Installation

### Requirements

- Python 2.7.x (to be adjusted to 3.6.x)
- Telegram Python API: [python-telegram-bot](https://github.com/python-telegram-bot/python-telegram-bot).

### Configuration File

A _config.json_ file is used to enable different features and specify their settings.

#### Telegram Bot Configuration

    "bot": {
        "token": "",
        "username": "",
        "first_name": "",
        "valid_chat_ids": [
        ]
    }

- `bot.token`: Telegram bot token obtained from the BotFather.
- `bot.username`: Telegram bot username, ends up in _Bot_ or _bot_.
- `bot.first_name`: Telegram bot name.
- `bot.valid_chat_ids`: Valid Telegram chat IDS. For cases where you don't want the whole world to use your bot.

#### Camera Configuration

    "camera": {
        "horizontal_flip": false,
        "vertical_flip": false,
        "resolution": [1280, 720],
        "rotation": 0,
        "video_duration": 5
    },

- `camera.horizontal_flip`: `true` to enable horizontal flip, `false` to disable.
- `camera.vertical_flip`: `true` to enable vertical flip, `false` to disable.
- `camera.resolution`: Resolution of image to capture, see [Raspberry Pi Camera Module](https://www.raspberrypi.org/documentation/raspbian/applications/camera.md) for supported values.
- `camera.rotation`: Rotation of image to capture, supported values are `0`, `90`, `180`, and `270`.
- `camera.video_duration`: Duration in seconds of video to capture.

#### Trivia Configuration

    "trivia": {
        "file_path": ""
    }

- `trivia.file_path`: File path for trivia. Expectation is one trivia per line.
