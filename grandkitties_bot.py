# -*- coding: utf-8 -*-

import sys
from os import remove
import argparse
import json
import logging
from datetime import datetime
from time import sleep
import subprocess
from random import randint
import telegram
from telegram.ext import Updater
from telegram.ext import CommandHandler
from telegram.ext import MessageHandler, Filters
try:
    import picamera
    is_picamera_available = True
except ImportError:
    is_picamera_available = False


logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)

# Global Variables
bot_valid_chat_ids = list()
video_duration = 1
trivia_file_path = ''


def load_json_to_dict(file_path):
    try:
        with open(file_path, 'r') as f_id:
            json_dict = json.load(f_id)
    except IOError:
        print 'ERROR: Cannot open file \'{0}\''.format(file_path)
        json_dict = ''
    except ValueError:
        print 'ERROR: Is \'{0}\' a JSON file?'.format(file_path)
        json_dict = ''

    return json_dict


def load_file_to_lines(file_path):
    try:
        with open(file_path, 'r') as f_id:
            file_lines = f_id.readlines()
    except IOError:
        print 'ERROR: Cannot open file \'{0}\''.format(file_path)
        file_lines = ''
    except ValueError:
        print 'ERROR: Cannot decode \'{0}\''.format(file_path)
        file_lines = ''

    return file_lines


def validate_config(config_dict):
    if 'bot' not in config_dict:
        sys.exit('ERROR: No \'bot\' field in JSON file!')
    else:
        bot_fields = ['token', 'username', 'first_name', 'valid_chat_ids']
        for field in bot_fields:
            if field not in config_dict['bot']:
                sys.exit('ERROR: No \'{0}\' subfield in \'bot\' field!'.format(field))
            elif not config_dict['bot'][field]:
                sys.exit('ERROR: \'{0}\' subfield is empty!'.format(field))


def configure_camera(config_dict):
    global video_duration
    if 'camera' in config_dict:
        if 'horizontal_flip' in config_dict['camera']:
            hflip = config_dict['camera']['horizontal_flip']
            if hflip is not True and hflip is not False:
                print 'WARNING: horizontal flip only allows true/false values.'
            else:
                camera.hflip = hflip
        if 'vertical_flip' in config_dict['camera']:
            vflip = config_dict['camera']['vertical_flip']
            if vflip is not True and vflip is not False:
                print 'WARNING: vertical flip only allows true/false values.'
            else:
                camera.vflip = vflip
        if 'rotation' in config_dict['camera'] and type(config_dict['camera']['rotation']) == int:
            camera.rotation = config_dict['camera']['rotation']
        if 'resolution' in config_dict['camera'] and len(config_dict['camera']['resolution']) == 2 \
                and type(config_dict['camera']['resolution'][0]) == int \
                and type(config_dict['camera']['resolution'][1]) == int:
            camera.resolution = (config_dict['camera']['resolution'][0], config_dict['camera']['resolution'][1])
        if 'video_duration' in config_dict['camera']:
            video_duration = config_dict['camera']['video_duration']


def get_timestamp():
    return datetime.now().strftime('%Y%m%d%A_%H%M%S')


def validate_chat_id(bot, update):
    # List of valid chat IDs
    global bot_valid_chat_ids

    # Get message and user
    message = update.message.text.encode('utf-8')
    user = update.message.chat.first_name.encode('utf-8')
    print '[{0}] Received message from \'{1}\': {2}'.format(get_timestamp(), user, message)

    if update.message.chat_id in bot_valid_chat_ids:
        return True
    else:
        bot.send_message(chat_id=update.message.chat_id, text='Unauthorized user!')
        return False


def send_action(bot, update, type):
    bot.send_chat_action(chat_id=update.message.chat_id, action=telegram.ChatAction.TYPING)
    bot.send_message(chat_id=update.message.chat_id, text='I\'m taking a {0}. Give me a moment...'.format(type))
    bot.send_chat_action(chat_id=update.message.chat_id, action=telegram.ChatAction.TYPING)


def send_message(bot, update, text, photo=None, video=None):
    if photo is None and video is None:
        bot.send_message(chat_id=update.message.chat_id, text=text)
    elif photo is not None and video is None:
        bot.send_photo(chat_id=update.message.chat_id, photo=photo, caption=text)
    elif video is not None and photo is None:
        bot.send_video(chat_id=update.message.chat_id, video=video, caption=text)


def command_start(bot, update):
    if validate_chat_id(bot, update) is True:
        send_message(bot, update, 'I\'m the Grandkitties bot!\n'
                                  '/photo: take a photo (beta)\n'
                                  '/video: take a short video (beta)\n'
                                  '/trivia: display trivia (alpha)')


def command_photo(bot, update):
    if validate_chat_id(bot, update) is True:
        # Send action to notify something is happening
        send_action(bot, update, 'photo')

        # If not on RPi, use Lenna. Otherwise, capture photo.
        if is_picamera_available is False:
            photo_path = 'Lenna.png'
            photo_caption = 'I was not able to capture a photo. Here is Lenna.'
        else:
            # Capture photo with timestamp
            timestamp = get_timestamp()
            photo_path = '{0}.png'.format(timestamp)
            camera.capture(photo_path)
            # Set caption
            photo_caption = 'This is {0}'.format(photo_path)

        # Try to open photo as binary
        try:
            photo_bin = open(photo_path, 'rb')
        except IOError:
            photo_bin.close()
            photo_bin = None
            photo_caption = 'There was an error opening photo. Sorry :/'

        # Send photo
        send_message(bot, update, photo_caption, photo=photo_bin)
        # Close photo handler
        photo_bin.close()
        # Remove photo if not Lenna
        if photo_path != 'Lenna.png':
            remove(photo_path)


def record_video(timestamp):
    try:
        h264_path = '{0}.h264'.format(timestamp)
        camera.start_recording(h264_path)
        sleep(video_duration)
        camera.stop_recording()
    except picamera.PiCameraError:
        h264_path = None

    return h264_path


def convert_h264_to_mp4(h264_path, timestamp):
    mp4_path = '{0}.mp4'.format(timestamp)
    command = 'MP4Box -add {0} {1}'.format(h264_path, mp4_path)

    try:
        output = subprocess.check_output(command, stderr=subprocess.STDOUT, shell=True)
    except subprocess.CalledProcessError as e:
        mp4_path = None
        print 'FAIL: command \'{0}\' failed with output \'{1}\''.format(e.cmd, e.output)

    return mp4_path


def command_video(bot, update):
    if validate_chat_id(bot, update) is True:
        # If not on RPi, do not send anything. Otherwise, capture H264 video, convert to MP4, and send.
        if is_picamera_available is False:
            send_message(bot, update, 'Sorry. Cannot send a video from my current location...')
        else:
            # Send action to notify something is happening
            send_action(bot, update, 'video')

            # Capture video with timestamp
            timestamp = get_timestamp()
            h264_path = record_video(timestamp)

            # Check if video was actually captured
            if h264_path is None:
                video_bin = None
                video_caption = 'There was an error capturing video. Sorry :/'
                mp4_path = None
            else:
                # Convert h264 to mp4
                mp4_path = convert_h264_to_mp4(h264_path, timestamp)

                # Set caption
                video_caption = 'This is {0}'.format(mp4_path)

                # Try to open video as binary
                try:
                    video_bin = open(mp4_path, 'rb')
                except IOError:
                    video_bin.close()
                    video_bin = None
                    video_caption = 'There was an error opening video. Sorry :/'

            # Send video
            send_message(bot, update, video_caption, video=video_bin)
            # Remove videos
            if h264_path is not None:
                remove(h264_path)
                # Close photo handler
                video_bin.close()
            if mp4_path is not None:
                remove(mp4_path)


def command_trivia(bot, update):
    if validate_chat_id(bot, update) is True:
        # Open file to lines
        trivia_lines = load_file_to_lines(trivia_file_path)
        if not trivia_lines:
            trivia_message = 'Sorry. Trivia is not available or not working right now...'
        else:
            # Select random line
            trivia_message = trivia_lines[randint(0, len(trivia_lines) - 1)]

        send_message(bot, update, trivia_message)


def echo(bot, update):
    if validate_chat_id(bot, update) is True:
        send_message(bot, update, 'Sorry. Cannot hold a conversation yet...\n¯\_(ツ)_/¯')


def command_unknown(bot, update):
    if validate_chat_id(bot, update) is True:
        send_message(bot, update, 'Invalid command!')


if __name__ == '__main__':
    # Create parser
    parser = argparse.ArgumentParser(description='Start Grandkitties Bot')
    parser.add_argument('config_json', help='Path to configuration JSON', type=str)
    # Get parser results
    args = parser.parse_args()

    # Open JSON file to dict
    config_dict = load_json_to_dict(args.config_json)
    if not config_dict:
        sys.exit('ERROR opening configuration JSON')

    # Validate configuration
    validate_config(config_dict)

    # Set trivia file path if available
    if 'trivia' in config_dict and 'file_path' in config_dict['trivia']:
        trivia_file_path = config_dict['trivia']['file_path']

    # If PiCamera is available, start camera
    if is_picamera_available is True:
        camera = picamera.PiCamera()
        # Configure camera
        configure_camera(config_dict)

    # Get token, username, and first name
    bot_token = config_dict['bot']['token']
    bot_username = config_dict['bot']['username']
    bot_first_name = config_dict['bot']['first_name']
    # Get valid chat IDs
    bot_valid_chat_ids = config_dict['bot']['valid_chat_ids']

    # Create instanse of Telegram Bot
    bot = telegram.Bot(token=bot_token)

    # Check if credentials are correct
    if bot_username != bot.username or bot_first_name != bot.first_name:
        sys.exit('ERROR: Bot name does not match...')

    # Create 'Updater' object
    updater = Updater(token=bot_token)
    # Create 'Dispatcher' object
    dispatcher = updater.dispatcher

    # Use 'CommandHandler' subclass to call 'start' function called every time the Bot receives a Telegram message that
    # contains the '/start' command
    start_handler = CommandHandler('start', command_start)
    dispatcher.add_handler(start_handler)

    # Like above but for '/photo' to take a photo
    photo_handler = CommandHandler('photo', command_photo)
    dispatcher.add_handler(photo_handler)

    # Like above but for '/video' to take a video
    video_handler = CommandHandler('video', command_video)
    dispatcher.add_handler(video_handler)

    # Like above but for '/trivia'
    trivia_handler = CommandHandler('trivia', command_trivia)
    dispatcher.add_handler(trivia_handler)

    # Reply to any message
    echo_handler = MessageHandler(Filters.text, echo)
    dispatcher.add_handler(echo_handler)

    # Unknown handler for unknown commands
    unknown_handler = MessageHandler(Filters.command, command_unknown)
    dispatcher.add_handler(unknown_handler)

    # Start bot
    updater.start_polling()
